$(window).scroll(function () {
  let contentHeadHeigh = $(".content-head").height();
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});

$('.slider-home').owlCarousel({
  loop:true,
  margin:30,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
})



$("header .content-head nav ul li.submenu > a span").click(function(){
  $("header .content-head nav ul li.submenu > ul").toggleClass("active");
});


$(".menu-mobile").click(function(){
  $("header").toggleClass("active");
});
$(".category-list li").click(function(){
  $(this).toggleClass("active");
});



$(".search-menu").click(function(){
  $("body").addClass("show-search");
});
$("span.ti-close").click(function(){
  $("body").removeClass("show-search");
});

$(".menu-mobile").click(function(){
  $(".width-menu").toggleClass("show-menu");
});





$(document).ready(function () {
  $('.camnhanKH').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    autoplay: true,
    autoplayTimeout:15000,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
})


  // slider thumnails
  $(document).ready(function() {

    var sync1 = $("#sync1");
    var sync3 = $("#sync3");
    var sync4 = $("#sync-carrer");
    var sync2 = $("#sync2");
    var slidesPerPage = 8; //globaly define number of elements per page
    var syncedSecondary = true;

    sync1.owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: true,
        
        autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
        dots: true,
        loop: true,
        responsiveRefreshRate: 200,
        navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    }).on('changed.owl.carousel', syncPosition);

    sync3.owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: true,

        autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
        dots: true,
        loop: true,
        responsiveRefreshRate: 200,
        navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    }).on('changed.owl.carousel', syncPosition);

    sync4.owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: true,

        autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
        dots: true,
        loop: true,
        responsiveRefreshRate: 200,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
        },

        navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    }).on('changed.owl.carousel', syncPosition);



    sync2
        .on('initialized.owl.carousel', function() {
            sync2.find(".owl-item").eq(0).addClass("current");
        })
        .owlCarousel({
            items: slidesPerPage,
            dots: true,
            nav: true,
            smartSpeed: 200,
            slideSpeed: 500,
            slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
            responsiveRefreshRate: 100
        }).on('changed.owl.carousel', syncPosition2);

    function syncPosition(el) {
        //if you set loop to false, you have to restore this next line
        //var current = el.item.index;

        //if you disable loop you have to comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - (el.item.count / 2) - .5);

        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }

        //end block

        sync2
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = sync2.find('.owl-item.active').length - 1;
        var start = sync2.find('.owl-item.active').first().index();
        var end = sync2.find('.owl-item.active').last().index();

        if (current > end) {
            sync2.data('owl.carousel').to(current, 100, true);
        }
        if (current < start) {
            sync2.data('owl.carousel').to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            sync1.data('owl.carousel').to(number, 100, true);
        }
    }

    sync2.on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).index();
        sync1.data('owl.carousel').to(number, 300, true);
    });
});






$(window).scroll(function () {
  let contentHeadHeigh = $(".content-head").height();
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});


$(".menu-mobile").click(function(){
  $("header").toggleClass("active");
});

$(".item-faqs").click(function(){
  $(".item-faqs").removeClass("active");
  $(this).addClass("active");
});


$(".expand-rating").click(function(){
  $(".list-more-rating").addClass("active");
  $('.expand-rating').addClass("expand-none");
});


$(".open-popupRating").click(function(){
  $(".login-popup").addClass("active");
});
$(".btn-form-rating").click(function(){
  $(".login-popup").removeClass("active");
});



function inVisible(element) {
  //Checking if the element is
  //visible in the viewport
  var WindowTop = $(window).scrollTop();
  var WindowBottom = WindowTop + $(window).height();
  var ElementTop = element.offset().top;
  var ElementBottom = ElementTop + element.height();
  //animating the element if it is
  //visible in the viewport
  if ((ElementBottom <= WindowBottom) && ElementTop >= WindowTop)
    animate(element);
}

function animate(element) {
  //Animating the element if not animated before
  if (!element.hasClass('ms-animated')) {
    var maxval = element.data('max');
    var html = element.html();
    element.addClass("ms-animated");
    $({
      countNum: element.html()
    }).animate({
      countNum: maxval
    }, {
      //duration 5 seconds
      duration: 5000,
      easing: 'linear',
      step: function() {
        element.html(Math.floor(this.countNum) + html);
      },
      complete: function() {
        element.html(this.countNum + html);
      }
    });
  }

}

//When the document is ready
$(function() {
  //This is triggered when the
  //user scrolls the page
  $(window).scroll(function() {
    //Checking if each items to animate are 
    //visible in the viewport
    $("h2[data-max]").each(function() {
      inVisible($(this));
    });
  })
});










const overlayTrigger = document.querySelector('.overlay-launch'),
    overlay = document.querySelector('.overlay'),
    tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    
const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
let player,
    source;



function onYouTubeIframeAPIReady(evt) {
    console.log(source);
    player = new YT.Player('player', {
        height: '390',
        width: '640'
    });
}


function launchOverlay(evt) {
    const $trigger = evt.target;
    source = $trigger.dataset.src;
    overlay.classList.toggle('_active');
    player.loadVideoById(source, 5, "large")


    if (!overlay.classList.contains('_active')) {
        player.stopVideo();
    } else {
        player.playVideo();
    }
}

overlayTrigger.addEventListener('click', launchOverlay);

